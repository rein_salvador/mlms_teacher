import {
    AsyncStorage,
} from 'react-native'
import axios from '../utils/axios';
import oauthConsts from '../const/auth';

let list = async () => {
  let settings = await AsyncStorage.getItem(oauthConsts.CREDS)
  axios.defaults.baseURL = JSON.parse(settings).url;
  return axios.get('api/feedbacks');
}

export {
  list
}