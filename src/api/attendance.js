import {
    AsyncStorage,
} from 'react-native'
import axios from '../utils/axios';
import oauthConsts from '../const/auth';

let today = async () => {
  let settings = await AsyncStorage.getItem(oauthConsts.CREDS)
  axios.defaults.baseURL = JSON.parse(settings).url;
  return axios.get('api/teacher/meetings/today');
}

let setMeeting = async (data) => {
  let settings = await AsyncStorage.getItem(oauthConsts.CREDS)
  axios.defaults.baseURL = JSON.parse(settings).url;
  return axios.post('api/teacher/meetings', {
    ...data
  });
}

export {
  today,
  setMeeting
}