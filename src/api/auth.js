import {
    AsyncStorage,
} from 'react-native'
import axios from '../utils/axios';
import oauthConsts from '../const/auth';


let login = async (data, fn, err) => {
  let settings = await AsyncStorage.getItem(oauthConsts.CREDS)

  axios.defaults.baseURL = JSON.parse(settings).url;
  return axios.post('oauth/token', {
    ...data
  }).then(async (data) => {
    await AsyncStorage.setItem(oauthConsts.TOKEN, data.data.access_token);
    fn.call(null, data);
  }, ({ response }) => err.call(null, response));
  
}

let isLoggedIn = async () => {
  let token = await AsyncStorage.getItem(oauthConsts.TOKEN)
  return token !== null
}

export {
  login,
  isLoggedIn
}