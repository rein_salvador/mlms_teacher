import {
    AsyncStorage,
} from 'react-native'
import axios from '../utils/axios';
import oauthConsts from '../const/auth';

let register = async (data) => {
  let settings = await AsyncStorage.getItem(oauthConsts.CREDS)
  console.log('settings', settings)
  axios.defaults.baseURL = JSON.parse(settings).url;
  return axios.post('api/register', data);
}

export {
  register
}