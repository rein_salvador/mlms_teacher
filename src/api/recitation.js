import {
    AsyncStorage,
} from 'react-native'
import axios from '../utils/axios';
import oauthConsts from '../const/auth';

let list = async () => {
  let settings = await AsyncStorage.getItem(oauthConsts.CREDS)
  axios.defaults.baseURL = JSON.parse(settings).url;
  return axios.get('api/teacher/recitations?paginate=');
}

let create = async (data) => {
  let settings = await AsyncStorage.getItem(oauthConsts.CREDS)
  axios.defaults.baseURL = JSON.parse(settings).url;
  return axios.post('api/teacher/recitations', data);
}

let answers = async (id) => {
  let settings = await AsyncStorage.getItem(oauthConsts.CREDS)
  axios.defaults.baseURL = JSON.parse(settings).url;
  return axios.get(`api/teacher/recitations/${id}`);
}

export {
  list,
  create,
  answers
}