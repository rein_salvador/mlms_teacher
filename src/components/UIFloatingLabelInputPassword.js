
import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import UIFloatingLabelInput from './UIFloatingLabelInput';

export default class UIFloatingLabelInputPassword extends Component {
  state = {
    show: false,
  }

  toggleEye() {
    this.setState({
      show: !this.state.show
    })
  }

  render() {

    let { disableEye } = this.props;
    let icon = this.state.show ? 'eye-slash' : 'eye';
    
    let eye = !disableEye && (
      <TouchableOpacity 
        style={styles.iconContainer}
        onPress={() => { this.toggleEye() } }
      >
        {/* <Icon name={icon} style={styles.icon} /> */}
      </TouchableOpacity>
    );

    return (
      <View style={styles.container}>
        <UIFloatingLabelInput
          {...this.props}
          secureTextEntry={!this.state.show}
        />
        {eye}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width:'100%'
  },
  iconContainer: {
    position: 'absolute',
    top: 40,
    right: 12,
  },
  icon: {
    fontSize: 18,
    color: '#FFFFFF'
  }
});