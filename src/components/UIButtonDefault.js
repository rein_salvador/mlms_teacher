import React, { Component } from 'react';
import {
  View,
  TouchableNativeFeedback,
  StyleSheet
} from 'react-native';
// import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

export default class ButtonDefault extends Component {
  render () {
    let {onPress, isRipple, rippleColor, children, style, containerStyle} = this.props;
    
    const enableRipple = typeof isRipple === 'undefined' ? true: isRipple;

    return (
      <View style={[
        styles.containerStyle,
        containerStyle
      ]}>
        <TouchableNativeFeedback
          onPress={onPress}
          background={ enableRipple ? TouchableNativeFeedback.Ripple(rippleColor || TouchableNativeFeedback.SelectableBackground()) : null }>
          
          <View style={[
              styles.button,
              style
            ]}>
            {children}
          </View>

        </TouchableNativeFeedback>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    overflow: 'hidden',
    borderRadius: 3,
    width: 300,
    height: 50,
    elevation: 2
  },
  button: {
    width: '100%',
    height: '100%',
    backgroundColor: '#192a56',
    borderColor: '#192a56',
    elevation: 1,
    justifyContent: 'center',
    marginBottom: 10,
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 3,
  }
});