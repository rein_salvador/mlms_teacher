import React, { Component } from 'react';
import {
  Text,
  StyleSheet
} from 'react-native';
// import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

export default class UIText extends Component {
  render () {
    let { style, children } = this.props;

    return (
      <Text
        style={[
        styles.text,
        style,
      ]}>
        {children}
      </Text>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Roboto-Light',
    textAlign: 'center',
    color: 'white',
    fontSize: 13
  }
});