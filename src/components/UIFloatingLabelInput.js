
import React, { Component } from 'react';
import {
  Text,
  View,
  TextInput,
  StyleSheet
} from 'react-native';

class FloatingLabelInput extends Component {
  state = {
    isFocused: false,
  };

  handleFocus = () => this.setState({ isFocused: true });
  handleBlur = () => this.setState({ isFocused: false });

  render() {
    const { error, label, value, containerStyle, ...props } = this.props;
    const { isFocused } = this.state;

    const moveToTop = !(isFocused || (value && value.length));
    const UIError = (
      <Text style={[styles.error, {
        display: error ? 'flex' : 'none',
      }]}>
        {error}
      </Text>
    );

    const dynamicStyle = StyleSheet.create({
      label: {
        top: moveToTop ? 40 : 15,
        fontSize: moveToTop ? 14 : 10,
        color: !isFocused ? '#cccccc' : '#192a56',
      },
      textInput: {
        borderBottomColor: !isFocused ? '#cccccc' : '#192a56'
      }
    });

    return (
      <View style={StyleSheet.flatten([
          styles.container,
          containerStyle,
        ])}>
        <Text style={[
          styles.label,
          dynamicStyle.label
        ]}> 
          {label}
        </Text>
        <TextInput
          {...props}
          style={[
            styles.textInput,
            dynamicStyle.textInput
          ]}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          value={value}
          underlineColorAndroid='transparent'
        />
        {UIError}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  error: {
    backgroundColor: '#B9302B',
    width: '100%',
    fontSize: 13,
    textAlign: 'center',
    color: '#FFFFFF'
  },
  container: {
    marginBottom: 0,
    paddingTop: 20,
    width: '100%',
  },
  label: {
    position: 'absolute',
    left: 10,
  },
  textInput: { 
    fontSize: 18,
    borderBottomWidth: 1,
    paddingLeft: 15,
    paddingBottom: 8,
    color: '#192a56',
  }
});

export default FloatingLabelInput;