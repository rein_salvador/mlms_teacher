const oauthConstants = {
  URL: '@base:url',
  TOKEN: '@auth:token',
  CREDS: '@auth:creds',
  USER: '@auth:user'
}
  
export default oauthConstants
    