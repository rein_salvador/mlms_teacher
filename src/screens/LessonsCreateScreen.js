import React from 'react';
import { 
  View, 
  TouchableOpacity,
  Text,
  ScrollView,
  StyleSheet,
  ActivityIndicator
} from 'react-native';
import * as lesson from '../api/lesson';
import {
  UIButton,
  UIText,
  UIFloatingLabelInput
} from '../components';
import { 
  DocumentPicker, 
  DocumentPickerUtil 
} from 'react-native-document-picker';


class LessonsCreateScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      file: '',
      title: '',
      description: '',
      pre_requisite_id: '',
      passing_score: '',

      errors: null,
      isLoading: false
    }
  }

  static navigationOptions = {
    title: 'Create Lesson'
  }

  save() {
    this.setState({isLoading: true})

    const data = new FormData();
    data.append('title', this.state.title);
    data.append('description', this.state.description);
    data.append('pre_requisite_id', this.state.pre_requisite_id);
    data.append('passing_score', this.state.passing_score);
    data.append('file', this.state.file);

    lesson.create(data).then((response) => {
      this.setState({isLoading: false})
      this.props.navigation.goBack()
    }, (err) => {
      this.setState({
        errors: null,
        errors: err.response.data.errors,
        isLoading: false
      })
    });
  }

  chooseFile() {
    DocumentPicker.show({
      filetype: [DocumentPickerUtil.allFiles()],
    },(error,res) => {
      if (res) {
        this.setState({
          file: {
            uri: res.uri,
            type: res.type,
            name: res.fileName
          }
        })
      }
    });
  }

  renderLoading() {
    if (this.state.isLoading) return (<ActivityIndicator size="small" color="#0000ff" />);
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={style.container}>
          {this.renderLoading()}
          <View style={style.content}>
            <UIFloatingLabelInput
              label='Title'
              error={this.state.errors && this.state.errors.title}
              value={this.state.title}
              onChangeText={title => this.setState({title}) }
            />
            <UIFloatingLabelInput
              label='Description'
              error={this.state.errors && this.state.errors.description}
              value={this.state.description}
              onChangeText={description => this.setState({description}) }
            />
            <UIFloatingLabelInput
              label='Passing score'
              error={this.state.errors && this.state.errors.passing_score}
              value={this.state.passing_score}
              onChangeText={passing_score => this.setState({passing_score}) }
            />
            <UIFloatingLabelInput
              label='Pre-requsite ID'
              error={this.state.errors && this.state.errors.pre_requisite_id}
              value={this.state.pre_requisite_id}
              onChangeText={pre_requisite_id => this.setState({pre_requisite_id}) }
            />

            <UIButton
              containerStyle={style.file}
              style={[
                style.fileContent,
                this.state.file && {
                  backgroundColor: '#2848ff87'
                } 
              ]}
              onPress={() => {this.chooseFile()}}
            >
              <UIText style={{color: '#333'}}>
                {this.state.file ? this.state.file.name : 'File'}
              </UIText>
            </UIButton>

            <UIButton
              containerStyle={{marginVertical:10}}
              onPress={() => {this.save()}}
            >
              <UIText>
                Save
              </UIText>
            </UIButton>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: { 
    alignItems: 'center',
    marginTop: 5
  },
  content: {
    width: '100%',
    paddingHorizontal: 15,
    alignItems: 'center',
  },

  file: {
    marginTop: 20,
    width: 80, 
    height: 80,
    borderRadius: 4,
    borderWidth: 1,
    borderStyle: 'dashed'
  },
  fileContent: {
    backgroundColor: '#EEE',
  }
});

export default LessonsCreateScreen;