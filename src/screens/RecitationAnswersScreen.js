import React from 'react';
import {
  View,
  Text, 
  TouchableOpacity,
  ToastAndroid,
  ScrollView, 
  StyleSheet
} from 'react-native';
import * as recitation from '../api/recitation';

class RecitationAnswersScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      answers: [],
    }
  }

  static navigationOptions = {
    title: 'Answers'
  }

  componentDidMount() {
    this.getRecitations();
  }

  getRecitations() {
    recitation.answers(this.props.navigation.state.params.id).then((response) => {
      this.setState({answers: response.data.data.answers})
    })
  }

  renderRecitationAnswers() {
    let answers = this.state.answers
    if (answers.length <= 0) return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={[style.title]}>
          No answers yet.
        </Text>
      </View>
    );

    answers = answers.map((answer, key) => {
      return (
        <TouchableOpacity
          key={key}
          style={style.listItem}
          >
          <View>
            <View style={{flex: 1}}>
              <Text style={[style.title]}>
                {answer.answer}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1}}>
              <Text>
                {answer.student.name}
              </Text>
            </View>
            <View>
              <Text>
                {answer.created_at}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      )
    })

    return (answers)
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ScrollView contentContainerStyle={style.container}>
          <View style={style.content}>
            <View style={style.attendances}>
              <View style={{
                flexDirection: 'column'
              }}>
                {this.renderRecitationAnswers()}
              </View>

            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: { 
    alignItems: 'center',
    marginTop: 5
  },
  content: {
    width: '100%',
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  attendances: {
    marginTop: 5,
    width: '100%',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 15,
    marginVertical: 3
  },
  listItem: {
    width: '100%',
    padding: 10,
    backgroundColor: '#FFF',
    marginBottom: 5,
  },
});

export default RecitationAnswersScreen;