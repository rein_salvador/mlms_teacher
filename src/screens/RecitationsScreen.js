import React from 'react';
import {
  View,
  Text, 
  TouchableOpacity,
  ToastAndroid,
  ScrollView, 
  StyleSheet
} from 'react-native';
import * as recitation from '../api/recitation';
import {
  UIButton,
  UIText,
  UIFloatingLabelInput
} from '../components';

class RecitationsScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      recitations: [],
    }
  }

  static navigationOptions = {
    title: 'Recitations'
  }

  componentDidMount() {
    this.getRecitations();
  }

  getRecitations() {
    recitation.list().then((response) => {
      this.setState({recitations: response.data.data.data})
    })
  }

  addRecitation() {
    recitation.create({
      question: this.state.question
    }).then((response) => {
      this.getRecitations()
      this.setState({question: ''})
    })
  }

  renderRecitations() {
    let recitations = this.state.recitations
    recitations = recitations.map((recitation, key) => {
      return (
        <TouchableOpacity
          key={key}
          style={style.listItem}
          onPress={()=> {
            this.props.navigation.navigate('RecitationAnswers', {
              id : recitation.id
            })
          }}
          >
          <View style={{flex: 1}}>
            <Text style={[style.title]}>
              {recitation.question}
            </Text>
          </View>
          <View>
            <Text>
              {recitation.created_at}
            </Text>
          </View>
        </TouchableOpacity>
      )
    })

    return (recitations)
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ScrollView contentContainerStyle={style.container}>
          <View style={style.content}>

            <UIFloatingLabelInput
              label='Question'
              value={this.state.question}
              onChangeText={question => this.setState({question}) }
            />

            <UIButton
              containerStyle={{marginTop:10}}
              onPress={() => {this.addRecitation()}}
            >
              <UIText>
                Create Recitation
              </UIText>
            </UIButton>
            <View style={style.attendances}>
              <Text style={style.title}>
                Recitations: 
              </Text>
              <View style={{
                flexDirection: 'column'
              }}>
                {this.renderRecitations()}
              </View>

            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: { 
    alignItems: 'center',
    marginTop: 5
  },
  content: {
    width: '100%',
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  attendances: {
    marginTop: 5,
    width: '100%',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 15,
    marginVertical: 3
  },
  listItem: {
    flexDirection: 'row',
    width: '100%',
    padding: 10,
    backgroundColor: '#FFF',
    marginBottom: 5,
  },
});

export default RecitationsScreen;