import React from 'react';
import { 
  View, 
  TextInput,
  TouchableOpacity,
  Text,
  ScrollView,
  StyleSheet,
  Linking
} from 'react-native';
import * as lesson from '../api/lesson';
import {
  UIButton,
  UIText
} from '../components';
import { 
  DocumentPicker, 
  DocumentPickerUtil 
} from 'react-native-document-picker';


class LessonsScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      lessons: []
    }
  }

  static navigationOptions = {
    title: 'Lessons'
  }

  componentDidMount() {
    lesson.list().then((response) => {
      this.setState({lessons: response.data.data.data})
    })
  }

  downloadLink(url) {
    Linking.openURL(url).catch(err => console.error('An error occurred', err));
  }

  renderLessons() {
    let lessons = this.state.lessons
    lessons = lessons.map((lesson, key) => {
      return (
        <TouchableOpacity
          key={key}
          style={style.listItem}
          onPress={() => {this.downloadLink(lesson.filename)}}
          >
          <View style={{flex: 1}}>
            <Text style={[style.title, style.listItemTitle]}>
              {lesson.title}
            </Text>
          </View>
          <View>
            <Text >
              ID: {lesson.id}
            </Text>
            <Text>
              {lesson.updated_at}
            </Text>
          </View>
        </TouchableOpacity>
      )
    })

    return (lessons)
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={style.container}>

          <View style={style.content}>
            <UIButton
              containerStyle={{marginVertical:10}}
              onPress={() => {this.props.navigation.navigate('LessonsCreate')}}
            >
              <UIText>
                Create Lesson
              </UIText>
            </UIButton>
            {this.renderLessons()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: { 
    alignItems: 'center',
    marginTop: 5
  },
  content: {
    width: '100%',
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  attendances: {
    marginTop: 5,
    width: '100%',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 15,
    marginVertical: 3
  },
  listItem: {
    flexDirection: 'row',
    width: '100%',
    padding: 10,
    backgroundColor: '#FFF',
    marginBottom: 5,
  },
});

export default LessonsScreen;