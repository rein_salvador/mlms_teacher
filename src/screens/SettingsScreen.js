import React from 'react';
import { 
  View, 
  StyleSheet,
  AsyncStorage,
  ToastAndroid,
} from 'react-native';
import authconst from '../const/auth';
import {
  UIButton,
  UIText,
  UIFloatingLabelInput
} from '../components';

class SettingsScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      url: 'http://192.168.254.145',
      client_id: 2,
      secret: 'secret'
    }
  }

  static navigationOptions = {
    title: 'Settings'
  }

  async save() {
    try {
      await AsyncStorage.setItem(authconst.CREDS, JSON.stringify(this.state))
      await AsyncStorage.setItem(authconst.URL, this.state.url)
      ToastAndroid.show('Settings has been save.', ToastAndroid.SHORT)
    } catch (error) {
    }
  }

  async componentDidMount() {

    try {
      let value = await AsyncStorage.getItem(authconst.CREDS) 
      if (value !== null) {
        this.setState(JSON.parse(value))
      }
    } catch (error) {
    }
  }

  render() {
    return (
      <View style={style.container}>
        <View style={style.content}>
          <UIFloatingLabelInput
            label='URL'
            value={this.state.url}
            onChangeText={url => this.setState({url}) }
          />

          <UIFloatingLabelInput
            label='Client ID'
            value={`${this.state.client_id}`}
            onChangeText={client_id => this.setState({client_id}) }
          />

          <UIFloatingLabelInput
            label='Client Secret'
            value={this.state.secret}
            onChangeText={secret => this.setState({secret}) }
          />
        </View>

        <UIButton
          onPress={() => {this.save()}}
        >
          <UIText>Save</UIText>
        </UIButton>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 20,
    marginTop: 20
  },
  content: {
    width:'100%', 
    marginBottom: 15,
  }
})

export default SettingsScreen;