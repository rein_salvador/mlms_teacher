import React from 'react';
import { 
  View,
  TouchableOpacity,
  Text,
  ScrollView,
  StyleSheet,
} from 'react-native';
import {
  UIButton,
  UIText
} from '../components';
import * as quiz from '../api/quiz';

class QuizzesScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      quizzes: []
    }
  }

  static navigationOptions = {
    title: 'Quizzes'
  }

  componentDidMount() {
    quiz.list().then((response) => {
      this.setState({quizzes: response.data.data.data})
    })
  }

  renderQuizzes() {
    let quizzes = this.state.quizzes
    quizzes = quizzes.map((quiz, key) => {
      return (
        <TouchableOpacity
          key={key}
          style={style.listItem}
          >
          <View style={{flex: 1}}>
            <Text style={[style.title, style.listItemTitle]}>
              {quiz.title}
            </Text>
          </View>
          <View>
            <Text >
              ID: {quiz.id}
            </Text>
            <Text>
              {quiz.created_at}
            </Text>
          </View>
        </TouchableOpacity>
      )
    })

    return (quizzes)
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={style.container}>

          <View style={style.content}>
            <UIButton
              containerStyle={{marginVertical:10}}
              onPress={() => {this.props.navigation.navigate('QuizCreate')}}
            >
              <UIText>
                Create Quiz
              </UIText>
            </UIButton>
            {this.renderQuizzes()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: { 
    alignItems: 'center',
    marginTop: 5
  },
  content: {
    width: '100%',
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  attendances: {
    marginTop: 5,
    width: '100%',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 15,
    marginVertical: 3
  },
  listItem: {
    flexDirection: 'row',
    width: '100%',
    padding: 10,
    backgroundColor: '#FFF',
    marginBottom: 5,
  },
});

export default QuizzesScreen;