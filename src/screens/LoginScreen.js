import React from 'react';
import { 
  View, 
  StyleSheet,
  Text,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import { 
  NavigationActions,
  StackActions
} from 'react-navigation';
import * as user from '../api/user';
import {
  UIButton,
  UIText,
  UIFloatingLabelInput,
  UIFloatingLabelInputPassword,
} from '../components';
import authconst from '../const/auth';
import * as auth from '../api/auth';

class LoginScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      password: '',
      user_type: 'teacher',
      errors: null,
      isLoading: false,
    }
  }

  static navigationOptions = {
    header: null
  }

  async login() {
    this.setState({isLoading: true})
    let settings = await AsyncStorage.getItem(authconst.CREDS);
    
    settings = JSON.parse(settings);
    
    auth.login({
      'grant_type': 'password',
      'client_id': settings.client_id,
      'client_secret':settings.secret,
      'username': this.state.email,
      'password': this.state.password,
      'scope': '',
    }, (data) => {
      this.goToHome()
    }, () => {
      ToastAndroid.show('Login failed', ToastAndroid.SHORT)
      this.setState({isLoading: false})
    })
  }

  goToHome() {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Home' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  async componentDidMount() {
    if (await auth.isLoggedIn()) this.goToHome()
  }

  renderLoading() {
    if (this.state.isLoading) return (<ActivityIndicator size="small" color="#0000ff" />);
  }

  render() {
    return (
      <View style={{flex:1}}>
        <ScrollView contentContainerStyle={style.container}>
          <View style={style.content}>

            <UIFloatingLabelInput
              label='Email'
              error={this.state.errors && this.state.errors.email}
              value={this.state.email}
              onChangeText={email => this.setState({email}) }
              />
            <UIFloatingLabelInputPassword
              label='Password'
              error={this.state.errors && this.state.errors.password}
              value={this.state.password}
              onChangeText={password => this.setState({password}) }
              />

            {this.renderLoading()}
            <UIButton
              containerStyle={{marginTop:10}}
              onPress={() => {this.login()}}
              >
              <UIText>
                Login
              </UIText>
            </UIButton>
            <UIButton
              containerStyle={{marginTop:10}}
              onPress={() => {this.props.navigation.navigate('Registration')}}
              >
              <UIText>
                Sign Up
              </UIText>
            </UIButton>
            <UIButton
              containerStyle={{marginTop:10}}
              onPress={() => {
                this.props.navigation.navigate('Settings')
              }}
            >
              <UIText>Settings</UIText>
            </UIButton>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: { 
    alignItems: 'center',
    marginTop: 5
  },
  content: {
    width: '100%',
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  attendances: {
    marginTop: 5,
    width: '100%',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 15,
    marginVertical: 3
  },
  listItem: {
    flexDirection: 'row',
    width: '100%',
    padding: 10,
    backgroundColor: '#FFF',
    marginBottom: 5,
  },
  listItemTitle: {
    flex: 60,
    fontSize: 14,
  },
});

export default LoginScreen;