import React from 'react';
import { 
  View, 
  StyleSheet,
  AsyncStorage,
  ToastAndroid,
  ActivityIndicator
} from 'react-native';
import {
  UIButton,
  UIText,
  UIFloatingLabelInput,
  UIFloatingLabelInputPassword
} from '../components';
import authconst from '../const/auth';
import * as auth from '../api/auth';
import { 
  NavigationActions,
  StackActions
} from 'react-navigation';

class HomeScreen extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      username: 'johndoe@mail.com',
      password: '123456',
      isLoggedIn: false,
      isLoading: false,
      settings: {}
    };
  }

  static navigationOptions = {
    title: 'MLMS Teacher'
  }
  
  renderButtons() {
    const screens = ['Attendances', 'Lessons', 'Quizzes', 'Recitations', 'Questions'];

    menuButtons = screens.map((val, key) => (
      <UIButton
        style={!this.state.isLoggedIn && style.disabledButton}
        key={key}
        containerStyle={style.button}
        onPress={() => {
          if (!this.state.isLoggedIn) {
            ToastAndroid.show('Please sign in first.', ToastAndroid.SHORT)
            return
          }

          this.props.navigation.navigate(val)
        }}
      >
        <UIText>{val}</UIText>
      </UIButton>
    ));

    return (menuButtons);
  }
  
  async loginAttempt() {
    this.setState({isLoading: true})
    let settings = await AsyncStorage.getItem(authconst.CREDS);
    
    settings = JSON.parse(settings);
    
    auth.login({
      'grant_type': 'password',
      'client_id': settings.client_id,
      'client_secret':settings.secret,
      'username': this.state.username,
      'password': this.state.password,
      'scope': '',
    }, (data) => {
      this.setState({
        isLoggedIn: true, 
        isLoading: false, 
      })
    }, () => {
      ToastAndroid.show('Login failed', ToastAndroid.SHORT)
    })
  }

  async componentDidMount() {

    if (await auth.isLoggedIn()) this.setState({isLoggedIn: true})

  }

  async loginLogout() {
    if (this.state.isLoggedIn) {
      await AsyncStorage.removeItem(authconst.TOKEN)
      this.goToLogin()
    } else {
      this.loginAttempt();
    }
  }

  goToLogin() {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Login' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  loginForm() {
    if (!this.state.isLoggedIn) {
      return (
        <View style={style.content}>

          <UIFloatingLabelInput
              label='Username'
              value={this.state.username}
              onChangeText={username => this.setState({username}) }
            />
    
          <UIFloatingLabelInputPassword
            label='Password'
            value={this.state.password}
            onChangeText={password => this.setState({password}) }
          />
        </View>
      );
    }
  }

  renderLoading() {
    if (this.state.isLoading) return (<ActivityIndicator size="small" color="#0000ff" />);
  }

  render() {
    return (
      <View style={style.container}>
        
        <View style={style.content}>
          {this.loginForm()}
          {this.renderLoading()}
          
          <UIButton
            containerStyle={{marginTop:10}}
            style={this.state.isLoggedIn ? style.logoutButton : style.loginButton}
            onPress={() => {this.loginLogout()}}
          >
            <UIText>
              {this.state.isLoggedIn ? 'Sign out' : 'Sign in'}
            </UIText>
          </UIButton>
        </View>
        {this.renderButtons()}
        
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  content: {
    width:'100%', 
    marginBottom: 15,
    alignItems: 'center',
  },
  disabledButton: {
    backgroundColor: '#808db1',
    borderColor: '#808db1',
  },
  button: {
    marginBottom: 10
  },
  loginButton: {
    backgroundColor: '#ce1a1a',
    borderColor: '#ce1a1a',
  },
  logoutButton: {
    backgroundColor: '#e03030',
    borderColor: '#e03030',
  }
});

export default HomeScreen;