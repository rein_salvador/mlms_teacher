import React from 'react';
import { 
  View, 
  StyleSheet,
  Text,
  ToastAndroid,
  ScrollView,
} from 'react-native';
import * as attendance from '../api/attendance';
import {
  UIButton,
  UIText,
  UIFloatingLabelInput,
} from '../components';

class AttendanceScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      meeting: null,
      code: '',
      attendances: []
    }
  }

  static navigationOptions = {
    title: 'Attendances'
  }

  componentDidMount() {
    attendance.today().then((response) => {
      this.setState({
        meeting: response.data.data,
        attendances: response.data.data && response.data.data.attendances,
        code: response.data.data && response.data.data.code
      })
    })
  }

  setMeeting() {
    attendance.setMeeting({
      code: this.state.code
    }).then((response) => {
      ToastAndroid.show('Meeting has been set.', ToastAndroid.SHORT);
      this.setState({meeting: response.data.data})
    })
  }

  renderStudents() {
    if (this.state.attendances) {
      let attendances = this.state.attendances;

      let students = attendances.map((student, key) => {
        let time = student.user.created_at.split(' ')[1];
        let late = student.is_late ? {
          color: 'red'
        } : {};

        return (
          <View
            key={key}
            style={style.listItem}>
            <Text style={[style.title, style.listItemTitle]}>
              {student.user.name}
            </Text>
            <Text style={style.listItemStatus}>
              Time-in: 
              <Text style={late}> {time}</Text>
            </Text>
          </View>
        )
      })

      return (students)
    }

  }

  render() {
    return (
      <View style={{flex:1}}>
        <ScrollView contentContainerStyle={style.container}>
          <View style={style.content}>
            <Text style={style.title}>
              Meeting today: 
            </Text>
            <Text style={{color: 'grey', paddingLeft: 5}}>
              {this.state.meeting ? 'Code: ' + this.state.meeting.code : 'No meeting yet.'}
            </Text>

            <UIFloatingLabelInput
              label='Code'
              value={this.state.code}
              onChangeText={code => this.setState({code}) }
            />

            <UIButton
              containerStyle={{marginTop:10}}
              onPress={() => {this.setMeeting()}}
            >
              <UIText>
                Set meeting
              </UIText>
            </UIButton>
            <View style={style.attendances}>
              <Text style={style.title}>
                Attendances: 
              </Text>
              <View style={{
                flexDirection: 'column'
              }}>
                {this.renderStudents()}

              </View>

            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: { 
    alignItems: 'center',
    marginTop: 5
  },
  content: {
    width: '100%',
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  attendances: {
    marginTop: 5,
    width: '100%',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 15,
    marginVertical: 3
  },
  listItem: {
    flexDirection: 'row',
    width: '100%',
    padding: 10,
    backgroundColor: '#FFF',
    marginBottom: 5,
  },
  listItemTitle: {
    flex: 60,
    fontSize: 14,
  },
});

export default AttendanceScreen;