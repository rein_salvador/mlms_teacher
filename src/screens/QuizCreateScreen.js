import React from 'react';
import { 
  View,
  ScrollView,
  StyleSheet,
  ActivityIndicator,
  Text,
  TouchableOpacity
} from 'react-native';
import * as quiz from '../api/quiz';
import {
  UIButton,
  UIText,
  UIFloatingLabelInput
} from '../components';

class QuizCreateScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      title: '',
      time_limit: '',
      lecture_id: '',
      questions: [],

      question_title: '',
      question_answer: '',
      question_choice: '',
      choice_key: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
      choices: {},

      errors: null,
      isLoading: false
    }
  }

  static navigationOptions = {
    title: 'Create Quiz'
  }

  save() {
    this.setState({isLoading: true})
    let {title, time_limit, questions} = this.state;
    quiz.create({
      title,
      time_limit,
      lecture_id: null,
      questions
    }).then((response) => {
      this.setState({isLoading: false})
      this.props.navigation.goBack()
    }, (err) => {
      this.setState({
        errors: null,
        errors: err.response.data.errors,
        isLoading: false
      })
    });
  }

  getQuestions() {
    let {questions} = this.state;
    let holderQuestions = []
    questions.map((question, key) => {
      let arrChoices = []

      question.choices.map((choice) => {
        arrChoices[choice.key] = choice.value
        return choice
      })
      holderQuestions.push({
        ...question,
        choices: arrChoices
      })

      return question
    })
    return holderQuestions
  }

  renderLoading() {
    if (this.state.isLoading) return (<ActivityIndicator size="small" color="#0000ff" />);
  }

  renderQuestions() {
    let questions = this.state.questions
    questions = questions.map((question, key) => {
      return (
        <View
          key={key}
          style={style.listItem}
          >
          <View style={{flex: 1}}>
            <Text style={[style.title, style.listItemTitle]}>
              {question.question}
            </Text>
            <View>
              {question.choices && this.renderChoices(question.choices)}
            </View>
          </View>
          <View>
            <Text >
              Answer: {question.answer}
            </Text>
            <TouchableOpacity style={{alignSelf : 'flex-end'}}>
              <Text style={{color: 'red'}}>remove</Text>
            </TouchableOpacity>
          </View>
        </View>
      )
    })

    return (questions)
  }

  addQuestion() {
    let {questions, question_title, question_answer, choices} = this.state
    questions.push({
      question: question_title,
      answer: question_answer,
      choices
    })
    this.setState({
      questions,
      question_answer: '',
      question_title: '',
      choices: {}
    })
  }

  addChoice() {
    let {choices, question_choice, choice_key} = this.state;
    let key = choice_key.charAt(Object.keys(choices).length)
    choices[key] = question_choice
    this.setState({
      choices,
      question_choice: ''
    })
  }

  renderChoices(questChoice) {
    let choices = questChoice ? questChoice: this.state.choices
    let renderRemove = typeof questChoice === 'undefined' && (
      <View>
        <TouchableOpacity style={{alignSelf : 'flex-end'}}>
          <Text style={{color: 'red'}}>remove</Text>
        </TouchableOpacity>
      </View>
    )

    choices = Object.keys(choices).map((key) => {
        return (
          <View
            key={key}
            style={style.listItemChoices}
            >
            <View style={{flex: 1}}>
              <Text>
                {key} | {choices[key]}
              </Text>
            </View>
            {/* {renderRemove} */}
          </View>
        )
    });

    return (choices)
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={style.container}>
          {this.renderLoading()}
          <View style={style.content}>
            <UIFloatingLabelInput
              label='Title'
              error={this.state.errors && this.state.errors.title}
              value={this.state.title}
              onChangeText={title => this.setState({title}) }
            />
            <UIFloatingLabelInput
              label='Time limit'
              error={this.state.errors && this.state.errors.time_limit}
              value={this.state.time_limit}
              onChangeText={time_limit => this.setState({time_limit}) }
            />

            <View style={style.questionContainer}>
              <View style={style.questions}>
                <UIFloatingLabelInput
                  label='Question'
                  value={this.state.question_title}
                  onChangeText={question_title => this.setState({question_title}) }
                />
                <View style={style.questionContainer}>
                  <Text style={style.title}>
                    Choices: 
                  </Text>
                  <UIFloatingLabelInput
                    label='Choice'
                    value={this.state.question_choice}
                    onChangeText={question_choice => this.setState({question_choice}) }
                  />
                  <UIButton
                    containerStyle={style.addChoiceButtonContainer}
                    style={style.addChoiceButton}
                    onPress={() => {this.addChoice()}}
                    >
                    <UIText>
                      Add Choice
                    </UIText>
                  </UIButton>
                  {this.renderChoices()}
                </View>
                <UIFloatingLabelInput
                  label='Answer'
                  value={this.state.question_answer}
                  onChangeText={question_answer => this.setState({question_answer}) }
                />
              </View>
              <UIButton
                containerStyle={style.addQuestionButtonContainer}
                style={style.addQuestionButton}
                onPress={() => {this.addQuestion()}}
                >
                <UIText>
                  Add Question
                </UIText>
              </UIButton>
              <Text style={style.title}>
                Questions: 
              </Text>
              {this.renderQuestions()}
            </View>

            <UIButton
              containerStyle={{marginVertical:10}}
              onPress={() => {this.save()}}
            >
              <UIText>
                Save
              </UIText>
            </UIButton>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: { 
    alignItems: 'center',
    marginTop: 5
  },
  listItem: {
    flexDirection: 'row',
    width: '100%',
    padding: 10,
    backgroundColor: '#FFF',
    marginVertical: 5,
  },
  listItemChoices: {
    flexDirection: 'row',
    width: '100%',
    padding: 2,
    backgroundColor: '#e6e6e6',
    marginVertical: 3,
    fontSize: 9,
  },
  content: {
    width: '100%',
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 15,
    marginTop: 10,
  },
  questionContainer: {
    flex: 1,
    width: '100%',
    paddingLeft: 10,
  },
  addQuestionButtonContainer: {
    marginVertical: 10, 
    width: 150, 
    alignSelf: 'center'
  },
  addChoiceButtonContainer: {
    marginVertical: 10, 
    width: 150, 
    alignSelf: 'flex-end'
  },
  addChoiceButton: {
    backgroundColor: '#2d91f5',
    borderColor: '#2d91f5',
  },
  addQuestionButton: {
    backgroundColor: 'red',
    borderColor: 'red',
  }
});

export default QuizCreateScreen;