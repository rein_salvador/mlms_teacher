import React from 'react';
import { 
  View, 
  TouchableOpacity,
  Text,
  ScrollView,
  StyleSheet,
  Linking
} from 'react-native';
import * as question from '../api/question';
import {
  UIButton,
  UIText
} from '../components';
import { 
  DocumentPicker, 
  DocumentPickerUtil 
} from 'react-native-document-picker';


class QuestionsScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      questions: []
    }
  }

  static navigationOptions = {
    title: 'Questions'
  }

  componentDidMount() {
    question.list().then((response) => {
      this.setState({questions: response.data.data.data})
    })
  }

  renderQuestions() {
    let questions = this.state.questions

    if (! questions) return

    questions = questions.map((question, key) => {
      return (
        <TouchableOpacity
          key={key}
          style={style.listItem}
          >
          <View style={{flex: 1}}>
            <Text style={[style.title, style.listItemTitle]}>
              {question.description}
            </Text>
          </View>
          <View>
            <Text >
              {question.student.name}
            </Text>
            <Text>
              {question.student.created_at}
            </Text>
          </View>
        </TouchableOpacity>
      )
    })
  
    return (questions)
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={style.container}>
          <View style={style.content}>
            {this.renderQuestions()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: { 
    alignItems: 'center',
    marginTop: 5
  },
  content: {
    width: '100%',
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  attendances: {
    marginTop: 5,
    width: '100%',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 15,
    marginVertical: 3
  },
  listItem: {
    flexDirection: 'row',
    width: '100%',
    padding: 10,
    backgroundColor: '#FFF',
    marginBottom: 5,
  },
});

export default QuestionsScreen;