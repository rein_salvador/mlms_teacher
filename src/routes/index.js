import * as Screen from '../screens';
  
import { createStackNavigator } from 'react-navigation';
//   import navTransitionConfig from '@src/styles/animation/navTransitionConfig';
  
const RootStack = createStackNavigator(
  {
    Home: {
      screen: Screen.HomeScreen,
    },
    Registration: {
      screen: Screen.RegistrationScreen,
    },
    Login: {
      screen: Screen.LoginScreen,
    },
    Settings: {
      screen: Screen.SettingsScreen,
    },
    Attendances: {
      screen: Screen.AttendancesScreen,
    },
    Lessons: {
      screen: Screen.LessonsScreen,
    },
      LessonsCreate: {
        screen: Screen.LessonsCreateScreen,
      },
    Quizzes: {
      screen: Screen.QuizzesScreen,
    },
      QuizCreate: {
        screen: Screen.QuizCreateScreen,
      },
    Recitations: {
      screen: Screen.RecitationsScreen,
    },
      RecitationAnswers: {
        screen: Screen.RecitationAnswersScreen,
      },
    Questions: {
      screen: Screen.QuestionsScreen,
    }
  },
  {
    initialRouteName: 'Login',
  //   transitionConfig: navTransitionConfig,
  //   headerMode: 'screen',
  //   navigationOptions: {
  //     headerStyle: {
  //       backgroundColor: '#f58422',
  //     },
  //     headerTintColor: '#fff',
  //     headerTitleStyle: {
  //       fontWeight: 'bold',
  //     },
  //   },
  }
);

export default RootStack;