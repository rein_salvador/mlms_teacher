/**
 * Override default axios behavior
 *
 * Add interceptors and configs
 */

import axios from 'axios'
import {
    AsyncStorage,
} from 'react-native'
import './oauth'
import oauthConsts from '../../const/auth';
// import Config from 'react-native-config'

// let baseURL = async () => {
//   console.log(settings)
//   return JSON.parse(settings).url;
// }
// let settings = await AsyncStorage.getItem(oauthConsts.CREDS)
// console.log(JSON.parse(settings).url);
// console.log(axios);
// console.log(baseURL());
// axios.defaults.baseURL = 'http://lms-dev.vm5q5xmnxc.ap-southeast-1.elasticbeanstalk.com';

export default axios;
// export default async () => {
// };
