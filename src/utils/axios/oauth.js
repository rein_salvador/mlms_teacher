import axios from 'axios';
import oauthConstants from '../../const/auth';
import {AsyncStorage} from 'react-native';

axios.interceptors.request.use(async (config) => {
  const token = await AsyncStorage.getItem(oauthConstants.TOKEN);
  if ( token != null ) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
}, (err) => {
  return Promise.reject(err);
});
